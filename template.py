import matplotlib.pyplot as plt
import os
import random as rnd
import glob as gb
import copy as cp
import numpy as np
import pickle as pkl
import multiprocessing as mlp

def solve_poly(a, b, c):
    roots = np.roots([a, b, c])
    x_range = np.linspace(np.min(roots), np.max(roots), 100)
    poly_func = np.poly1d([a, b, c])
    y_range = poly_func(x_range)
    x_1, x_2 = roots
    return x_1, x_2, x_range, y_range

def read_file(file_name):
    with open(file_name, 'r') as f:
        plik = f.readlines()
    return plik

def write_to_file(nazwa_pliku, dane_x, dane_y):
    np.savetxt(nazwa_pliku, list(zip(dane_x, dane_y)), delimiter=', ')


def read_data_file(data_file_name):
    X, Y = np.loadtxt(data_file_name, delimiter=',', unpack=True)
    return X, Y

def plot_data(plot_file_name, os_x, os_y, def_func = plt.plot):
    def_func(os_x, os_y, label = 'funckja')
    plt.title('Wykres Funkcji')
    plt.xlabel('os X')
    plt.ylabel('os Y')
    plt.legend()
    plt.grid()
    plt.savefig(plot_file_name)
    plt.close()


def worker(zadanie):
    rozwiązania = []
    for i, case in enumerate(zadanie):
        x1, x2, odciete, rzedne = black_box(case, i)
        rozwiązania.append([x1, x2, odciete, rzedne])
    return rozwiązania

def black_box(case, i):
    p1, p2, p3 = [float(x) for x in case.split(',')]
    x1, x2, odciete, rzedne= solve_poly(p1, p2, p3)
    dir_name = 'poly_'+str(i)
    try:
        os.mkdir(dir_name)
    except:
        pass
    nazwa_zapisu = '_'.join([str(i), 'dane.txt'])
    nazwa_zapisu = '/'.join([dir_name, nazwa_zapisu])
    write_to_file(nazwa_zapisu, odciete, rzedne)
    X, Y = read_data_file(nazwa_zapisu)
    nazwa_wykres = '_'.join([str(i), 'wykres'])
    nazwa_wykres = '/'.join([dir_name, nazwa_wykres])
    plot_data(nazwa_wykres, X, Y)
    return x1, x2, odciete, rzedne

def poly_rng(default_arg = 3):
    params = [rnd.normalvariate(0, 15) for x in range(default_arg)]
    return params

def gen_input_file(file_to_save, poly_number):
    with open(file_to_save, 'w') as f:
        for nr in range(poly_number):
            z, x1, x2 = poly_rng()
            a, b, c = z, z*(x1+x2), z*x1*x2
            f.writelines(', '.join([str(x) for x in [a,b,c]])+'\n')

def make_data_base(selected_data):
    data_base = {}
    for x in selected_data:
        name = x.split('/')[0]
        data_base[name] = read_data_file(x)
    return data_base

def add_noise_to_base(some_base):
    for x in some_base:
        for i, val in enumerate(some_base[x][1]):
            some_base[x][1][i] = val + rnd.normalvariate(0, 5)
    return some_base

def unpack_base(base, file_name):
    for key in base:
        file_path = '/'.join([key, file_name])
        write_to_file(file_path, *base[key])
        plot_path = '/'.join([key, 'noise'])
        plot_data(plot_path, *base[key], def_func=plt.scatter)


def save_obj(obj, name):
    with open(name, 'wb') as f:
        pkl.dump(obj, f, pkl.HIGHEST_PROTOCOL)


def load_obj(name):
    with open(name, 'rb') as f:
        return pkl.load(f)


def prep_work(lista_zadan):
    zadanie_prep = []
    for i, x in enumerate(lista_zadan):
        zadanie_prep.append([x, i])
    return zadanie_prep


def multi_proc(funkcja, zadanie):
    rdzenie = mlp.cpu_count()
    pool = mlp.Pool(rdzenie)
    results = pool.starmap(funkcja, zadanie)
    return results

params = 'parametry.txt'
# gen_input_file(params, 10)
wejscie = read_file(params)
### Mogłoby być jedną funkcją 
zadanie_prep = prep_work(wejscie)
wyniki = multi_proc(black_box, zadanie_prep)

## single code
wyjście = worker(wejscie)

all_data = gb.glob('*/*dane.txt')

new_base = make_data_base(all_data)

noise_base = cp.deepcopy(new_base)

noise_base = add_noise_to_base(noise_base)
    
unpack_base(noise_base, 'noise.txt')

save_obj(new_base, 'new_base.pkl')
save_obj(noise_base, 'noise_base.pkl')

noise_base = load_obj('noise_base.pkl')
new_base = load_obj('new_base.pkl')
